#pragma once

#include <vector>
#include <qgraphicsview.h>
#include <qmenu.h>
#include <qfiledialog.h>
#include <qdebug.h>
#include <qscopedpointer.h>
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneWheelEvent>
#include <qpen.h>
#include <QWheelEvent>
#include <QPointF>

class ImageView : public QGraphicsView {
    Q_OBJECT
public:
	ImageView(QWidget* parent = nullptr);

	~ImageView();

	void setImage(const QImage& img, bool centerOnImage = true);

	bool hasImage() const;

	/*
	@return 当前的可见窗口的外接矩形
	*/
	QRectF sceneWindow() const;

	void centerOn(const QPointF& pos);

	void centerOn(qreal ax, qreal ay);

	/*
	@brief 设置缩放比例
	@param scale 缩放比例的数值会被限制在0.01在100之间
	*/
	void setScale(double scale);

	/*
	@brief 平移操作
	*/
	void move(QPointF viewDelta);

	/*@brief 通过缩放与移动使得矩形框能适应控件大小*/
	void fitViewport(double x, double y, double w, double h);

	void centerOn(const QGraphicsItem* item);

	/*
	@brief 获得鼠标在图像上的位置
	*/
	QPointF getCursorPosition() const;

signals:
	void imageChanged();
	void windowChanged();

public slots:
	
	/*@brief 视窗中心与图像中心对齐*/
	void centerOnImgCenter();

	/*
	@brief 设置缩放比例为1：1，然后居中
	*/
	void resetScale();

	/*@brief 使图像适应视窗大小*/
	void fitViewport();

	void removeImage();

protected:
	double scaleLmtLow = 0.01, scaleLmtUp = 60;

	void setTransform(const QTransform& matrix, bool combined = false);

	void wheelEvent(QWheelEvent* wheelEvent) override;

	void mouseMoveEvent(QMouseEvent* event) override;

	void mousePressEvent(QMouseEvent* event) override;

	void mouseReleaseEvent(QMouseEvent* event) override;

	void leaveEvent(QEvent* event) override;

	void scrollContentsBy(int dx, int dy) override;

	void contextMenuEvent(QContextMenuEvent* event) override;

	void resizeEvent(QResizeEvent* event) override;

private:
	class Private;
	Private* d;
};
