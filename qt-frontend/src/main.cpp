#include "mainwindow.hpp"
#include <QApplication>


int main(int argc, char* argv[]) {
    printf("Emulator start. \n");
	QApplication app(argc, argv);
	MainWindow mainWindow;
	mainWindow.show();
	return app.exec();
}
