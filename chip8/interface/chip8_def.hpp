#pragma once

#define SDELETE(p) do{if(p){delete p; p = 0;}} while(0)

namespace chip8 {
    using uint8 = unsigned char;
    using uint16 = unsigned short;

    enum class Chip8Error{
        OK,
        UnrecognizedOpcode,
        UndefinedError,
        NotImplementedYet,
        ROMError,
        StackError,
        IndexOutOfRange,
    };
};