CHIP-8是一种由Joseph Weisbecker开发的解释型编程语言。用该语言编写的程序可以在CHIP-8虚拟机上运行。CHIP-8虚拟机结构简单，研究它有助于程序员了解虚拟机/模拟器的基本设计方式和工作原理。

该C++程序实现了一个CHIP-8的虚拟机，并提供了一个简单的基于Qt的界面。使用键盘上的1、2、3、4、Q、W、E、R、A、S、D、F、Z、X、C、V这十六个键来操作。

程序使用CMake脚本来管理编译。

# 程序界面
![程序界面](./screen-shot.png)

# 参考文档
- [How to write an emulator chip-8 interpreter](http://www.multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/)
